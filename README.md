# CDK Miniproject

A Lambda function which takes files in one S3 bucket, encrypts them, and exports to another S3 bucket.

## Installation

Clone the git repo:

```bash
git clone https://gitlab.com/dukeaiml/IDS721/hc337-miniproject-3.git
```

Add your AWS credentials to your shell to link AWS to the CDK project.
Ensure that the credential pair that you pass in has the `Full Administrator` permission on AWS.

Run `cdk bootstrap` to set up the project, and then `cdk deploy` to publish changes to your AWS infrastructure.

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.
